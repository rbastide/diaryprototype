package Diary;

/**
 *
 * @author rzgheib
 */
public class Week {

	public static final int BOOKABLE_DAYS_PER_WEEK = 5;

	// A week number within a particular year (0-52).
	private final int weekNumber;

	public Week(int weekNumber) {
		this.weekNumber = weekNumber;
	}

	public void showAppointments() {
		// TODO implémenter cette méthode
		throw new UnsupportedOperationException("Pas encore implémenté")
	}

	public Day getDay(int dayInWeek) {
		// TODO implémenter cette méthode
		throw new UnsupportedOperationException("Pas encore implémenté")
	}

	/**
	 * @return The week number (0-52).
	 */
	public int getWeekNumber() {
		return weekNumber;
	}

}
