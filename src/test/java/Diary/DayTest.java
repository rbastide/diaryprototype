package test.java.Diary;

import Diary.Appointment;
import Diary.Day;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

public class DayTest {

	@Before
	public void setUp() {
	}

	@After
	public void tearDown() {
	}

	/**
	 * Test basic functionality by booking at either end of a day, and in the middle.
	 */
	@Test
	public void testMakeThreeAppointments() {
		Day day1 = new Day(1);
		Appointment appointm1 = new Appointment("Java lecture", 1);
		Appointment appointm2 = new Appointment("Java class", 1);
		Appointment appointm3 = new Appointment("Meet John", 1);
		assertEquals(true, day1.makeAppointment(9, appointm1));
		assertEquals(true, day1.makeAppointment(13, appointm2));
		assertEquals(true, day1.makeAppointment(17, appointm3));
	}

	/**
	 * Check that double-booking is not permitted.
	 */
	@Test
	public void testDoubleBooking() {
		Day day1 = new Day(1);
		Appointment appointm1 = new Appointment("Java lecture", 1);
		Appointment appointm2 = new Appointment("Error", 1);
		assertEquals(true, day1.makeAppointment(9, appointm1));
		assertEquals(false, day1.makeAppointment(9, appointm2));
	}
}
